#!/usr/bin/env python3

import time
import sys
import json

import requests

from requests.packages.urllib3.exceptions import InsecureRequestWarning

# python does not seem to trust certificates from digicert, (web browsers do)
# this is here to disable the warning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

bulktest = [
    "https://nl.bab.la/",
    "https://www.linguee.com/",
    "https://www.mijnwoordenboek.nl/",
    "https://context.reverso.net/",
    "https://dictionary.cambridge.org/",
    "https://www.merriam-webster.com/",
    "https://www.python.org/",
    "https://nl.wikipedia.org/",
    "https://www.codecademy.com/",
    "https://www.google.com/",
    "https://pypi.org/",
    "https://realpython.com/",
    "https://www.w3schools.com/",
]

try:
    from credentials import myauth
except ModuleNotFoundError:
    print("Put your credentials in a file called credentials.py like this:");
    print();
    print('myauth=("yourname", "yourpassword")');
    sys.exit(3);

class Request:
    """
    Request class for convenience
    """

    def __init__(self, json_str):
        """
        constructor
        """
        self.__name = json_str['name'];
        self.__request_id = json_str['request_id'];
        self.__request_type = json_str['request_type'];
        self.__status = json_str['status'];
        self.__submit_date = json_str['submit_date'];
        self.__finished_date = json_str['finished_date'];

    @property
    def name(self):
        return self.__name

    @property
    def request_id(self):
        return self.__request_id

    @property
    def request_type(self):
        return self.__request_type

    @property
    def status(self):
        return self.__status

    def __str__(self):
        return f"{self.__name} : {self.__request_id} {self.__status}";

class NewRequest:
    """
    Request class for registering new queries
    """

    def __init__(self, name, tp, domains):
        """ constructor """
        self.name = name
        self.type = tp
        self.domains = domains

    def to_json(self):
        """ convert to json representation """
        return json.dumps(self.__dict__)

def check_response(response, member):
    if response.status_code != 200:
        print(f"{response.status_code} : {response.reason}")
        print(response.content)
        sys.exit(1)
    try:
        obj = response.json()
        mem = obj[member];
    except Exception as e:
        print(f"Error : {e}");
        sys.exit(2)

    return mem

class BatchTester:
    """
    Main testing class
    """

    def __init__(self, baseurl):
        self.server = baseurl + "/api/batch/v2";

    # http://redocly.github.io/redoc/?url=https://batch.internet.nl/api/batch/openapi.yaml#operation/list_requests
    def list_requests(self):
        url = self.server + '/requests'
        response = requests.get(url, auth=myauth, verify=False)
        msg = check_response(response, "requests")
        return msg

    # http://redocly.github.io/redoc/?url=https://batch.internet.nl/api/batch/openapi.yaml#operation/register_request
    def register_request(self, name, req_type, domains):
        url = f"{self.server}/requests"
        new_request = NewRequest(name, req_type, domains)
        data = new_request.to_json()
        response = requests.post(url, auth=myauth, data=data, verify=False)
        msg = check_response(response, "request")
        return msg['request_id']

    def register_mail_request(self, name, domains):
        return self.register_request(self, name, "mail", domains)

    def register_web_request(self, name, domains):
        return self.register_request(self, name, "web", domains)

    # http://redocly.github.io/redoc/?url=https://batch.internet.nl/api/batch/openapi.yaml#operation/results
    def results(self, req_id):
        url = f"{self.server}/requests/{req_id}/results";
        response = requests.get(url, auth=myauth, verify=False)
        msg = check_response(response, "request")
        domains = check_response(response, "domains")
        return msg, domains

    # http://redocly.github.io/redoc/?url=https://batch.internet.nl/api/batch/openapi.yaml#operation/status_request
    def request_status(self, req_id):
        url = f"{self.server}/requests/{req_id}";
        response = requests.get(url, auth=myauth, verify=False)
        msg = check_response(response, "request")
        return msg['status']

    def sync_result(self, name, req_type, domains):
        req_id = self.register_request(name, req_type, domains)
        #req_id="b3d47e760a9845ff84c884c93291c0e7"

        status = None
        while status != 'done':
            status = self.request_status(req_id);
            print(f"state now : {status}");
            time.sleep(3)

        return self.results(req_id)

bt = BatchTester("https://cyberscanner.nl");

lr = bt.list_requests()
print("requests :\n-------------------------------------")
for e in lr:
    print(f" {e['request_id']} : {e['name']} is {e['status']}");

#msg, dom = bt.sync_result("generictest", "mail", [ "www.google.com" ])
#msg, dom = bt.sync_result("generictest", "web", bulktest )
#msg, dom = bt.sync_result("generictest", "mail", bulktest )

#for d in dom:
    #obj=dom[d]
    #print(f"{d} : {obj['status']} {obj['scoring']}")
